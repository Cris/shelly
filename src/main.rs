use colored::Colorize;
mod alias;
use alias::get_origin;
use rustyline::error::ReadlineError;
use rustyline::{Editor, Result};
use std::collections::HashMap;
use std::{
    env::{self, set_current_dir},
    io::{stdout, Write},
    path::Path,
    process::{Command, exit},
};
#[derive(Debug)]
struct CrisComm {
    command: String,
    arg: Vec<String>,
}

static DEBUG:bool = false;

fn main() -> Result<()> {
    let mut _home = String::new(); 
    match env::home_dir() {
        Some(homedir) => {_home = homedir.to_string_lossy().to_string();
    }
        None => {println!("Error: Could not find home_dir.");exit(0)}
    }
    let mut command_history = Vec::new();
    let mut alias_map:HashMap<String, String> = HashMap::new();
    let mut getter = Editor::<()>::new()?;
    if getter.load_history(&format!("{}/.config/.cr_his.txt",&_home)).is_err() {
        println!("No previous history.");
    }
    loop {
        stdout().flush().unwrap();

        // stdin().read_line(&mut input).unwrap();
        let input = getter.readline(format!(
            "{0} {1} {2} \n {3}",
            String::from("Shelly").green().bold(),
            String::from("at"),
            env::current_dir().unwrap().to_string_lossy().blue().bold(),
            String::from(">> ")
            ).as_str());

        match input {
            Ok(line) => {
                getter.add_history_entry(line.as_str());
                let _dir = _home.clone();
                let commands = line.trim().split(" && ").peekable();
                for inp_comm in commands {

                // let mut prev_comm = None;
                let sub_comms = inp_comm.trim().split(" | ").peekable();
                if inp_comm.trim().split(" | ").collect::<Vec<&str>>().len() == 1{
                let parts:Vec<String> = inp_comm.replace("~/", format!("{}/",&_home).as_str()).split_whitespace().map(|a| a.to_string()).collect();
                let command = parts.first().unwrap();
                let mut part = parts.clone();
                part.remove(0);
                let arg = part;
                command_history.push(CrisComm {
                command:get_origin(command.to_string(), &alias_map),
                arg,
                });
                let _prev_comm = CrisComm {
                    command:String::new(),
                    arg:Vec::new(),
                };
                exec(&mut alias_map,command_history.last().unwrap(),&command_history);
                match getter.save_history(&format!("{}/.config/.cr_his.txt",&_home)) {
                    Ok(_) => {
                    }
                    Err(_) => {
                        eprintln!("{}","Failed to save history.".red());
                    }
                }
            }
            else {
                for _sub_comm in sub_comms {
                    }
                }
            }
            },
            Err(ReadlineError::Interrupted) => {
            },
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
            },
            Err(err) => {
                println!("Error: {err:?}");
            }
        }
    }
}

fn exec(mut map: &mut HashMap<String,String>,command:&CrisComm,history:&Vec<CrisComm>) {
    if DEBUG {
        dbg!(&command.command);
    }
    let arg = &command.arg;
    if DEBUG {
        dbg!(arg);
    }
        match command.command.as_str() {
            "alias" => {
                let mut args = String::new();
                for tr in command.arg.iter() {
                    args = format!("{}{} ",args,tr);
                }
                // dbg!(args);
                let mut args_parsed = args.split('=');
                let aliasd = args_parsed.next();
                match aliasd {
                    Some(str) => {
                        let aliasd = str.trim().to_string();
                        let orid = args_parsed.next();
                        match orid {
                            Some(str) => {
                                let orid = str.trim().to_string();
                                // println!("{0}, {1}",aliasd,orid)
                                map.insert(aliasd, orid);
                            },
                            None => panic!(),
                        }
                    },
                    None => panic!(),
                }
            }
            "csdbg" => {
                match command.arg.last() {
                    Some(arg) => {
                        match arg.as_str() {
                        "command" => {
                            if history.len() >= 2 {
                            dbg!(history.get(history.len() - 2));
                            }
                        }
                        "history" => {
                            dbg!(history);
                        }
                        _ => {
                            eprintln!("{}","Something wrong had happened.".red());
                            }
                        }
                    },
                    None => {
                        eprintln!("{}","No argument provided...".red());
                    }
                }
            }

            "cd" => {
                if arg.clone().last().unwrap() == "--help" {
                    eprintln!("I am a teapot!");
                } else {
                    let new_dir = arg.iter().peekable().peek().map_or("/", |x| x);
                    let root = Path::new(new_dir);
                    if new_dir == "~/" {
                        set_current_dir(env::home_dir().unwrap()).unwrap();
                    } else {
                        set_current_dir(root).unwrap();
                    }
                }
            }
            "prev" => {

            }
            "exit" => exit(0),

            "quit" => exit(0),

            "about" => {
                println!(
                    "A funny {} Terminal Simunator, thanks for using me.",
                    "Toy".red()
                );
            }

            "help" => {
                println!("{0} \nProudly powered by excellent {1}\n","version: 0.1_alpha".red(),"Rustyline".bright_cyan().bold());
            }
            command => {
                let screl = arg.clone();
                let child = Command::new(command).args(screl).spawn();
                match child {
                    Ok(mut child) => {
                        child.wait().unwrap();
                    }
                    Err(e) => {
                        eprintln!("! {}",e.to_string().red());
                    }
                };
            } 
    }
}
